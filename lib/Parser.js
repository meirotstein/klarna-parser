var
  _ = require('lodash'),
  tokenizer = require('./Tokenizer');

/**
 * The syntax tree defines how to convert a set of ordered tokens into a valid expression in out 'language' - in our case it is a number
 *
 * @type {Object}
 * @readonly
 */
var syntaxTree = {};
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.LEFT_RIGHT, tokenizer.TYPES.LEFT_BOTTOM_RIGHT], "0");
_.set(syntaxTree, [tokenizer.TYPES.BLANK, tokenizer.TYPES.RIGHT, tokenizer.TYPES.RIGHT], "1");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.BOTTOM_RIGHT, tokenizer.TYPES.LEFT_BOTTOM], "2");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.BOTTOM_RIGHT, tokenizer.TYPES.BOTTOM_RIGHT], "3");
_.set(syntaxTree, [tokenizer.TYPES.BLANK, tokenizer.TYPES.LEFT_BOTTOM_RIGHT, tokenizer.TYPES.RIGHT], "4");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.LEFT_BOTTOM, tokenizer.TYPES.BOTTOM_RIGHT], "5");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.LEFT_BOTTOM, tokenizer.TYPES.LEFT_BOTTOM_RIGHT], "6");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.RIGHT, tokenizer.TYPES.RIGHT], "7");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.LEFT_BOTTOM_RIGHT, tokenizer.TYPES.LEFT_BOTTOM_RIGHT], "8");
_.set(syntaxTree, [tokenizer.TYPES.BOTTOM, tokenizer.TYPES.LEFT_BOTTOM_RIGHT, tokenizer.TYPES.BOTTOM_RIGHT], "9");

/**
 * @const
 * @type {number}
 */
var AMOUNT_OF_TOKENS_IN_ROW = 10;  //num of tokens for line including newline

/**
 * Wrapper for utilities functions uses by the Parser class
 * @type {object}
 */
var ParserUtils = {
  /**
   * returns the next position of the first line character starting from a given index
   * @param {Token[]} tokens
   * @param {number} index
   * @returns {number}
   */
  getIndexOfNextLine : function(tokens, index) {
    var indexToReturn = index + 1;

    while(indexToReturn < tokens.length && tokens[indexToReturn].type === tokenizer.TYPES.NEWLINE){
      ++indexToReturn;
    }

    return indexToReturn;
  }
};

/**
 * Node class
 * Represents a value that has been resolved from a set of Tokens
 * @param {staring} value value of node
 * @class
 * @constructor
 */
var Node = function(value) {
  this.value = value;
};

/**
 * Parser converts set of tokens into a set of nodes
 * @class
 * @public
 * @constructor
 */
var Parser = function() {};

/**
 * value of node which is result of unrecognized set of tokens
 * @const
 * @type {string}
 */
Parser.NODE_INVALID = "-1";

/**
 * Parse a set of Tokens into a matrix of nodes, which each row can be resolve into an invoice number
 * an invoice number in the document is constructed from three rows of tokens (ending with a newline)
 * this function uses three cursors - one of each row of tokens and it moves it simultaneously
 * for each move it resolve the three pointed tokens into a number
 *
 * @public
 * @param {Token[]} tokens
 * @returns {{lines: Array<Node[]>}}
 */
Parser.prototype.parse = function(tokens) {
  //skip input size check - assume that the input is valid

  var
    firstRowCursor = 0,
    secondRowCursorOffset = AMOUNT_OF_TOKENS_IN_ROW,
    thirdRowCursorOffset = AMOUNT_OF_TOKENS_IN_ROW * 2,
    lines = [],
    row = [];

  while(firstRowCursor + thirdRowCursorOffset < tokens.length) {

    var nodeTokensTypes = [
      tokens[firstRowCursor].type,
      tokens[firstRowCursor + secondRowCursorOffset].type,
      tokens[firstRowCursor + thirdRowCursorOffset].type
    ];

    row.push(new Node(_.get(syntaxTree , nodeTokensTypes) || Parser.NODE_INVALID));

    if(row.length === AMOUNT_OF_TOKENS_IN_ROW - 1) {
      lines.push(row);
      row = [];
      firstRowCursor = ParserUtils.getIndexOfNextLine(tokens, firstRowCursor + thirdRowCursorOffset);
    } else {
      ++firstRowCursor;
    }
  }

  return {
    lines: lines
  }
};

module.exports = Parser;