var
  _ = require('lodash'),
  Promise = require('bluebird'),
  fs = Promise.promisifyAll(require("fs"));

/**
 * utilities functions
 * these utilities are not necessarily used by the klarna-parser module itself but by some surrounding code
 * @type {object}
 */
module.exports = {

  /**
   * Reads files from fs
   * @public
   * @param {string} filePath path and name of file to read
   * @returns {Promise}
   */
  readFile: function(filePath) {
    return fs.readFileAsync(filePath, 'utf-8');
  },

  /**
   * converts set of nodes into a string
   * This is used by test and wrapper
   * As this logic is not defined as part of the parser component itself it can be easily
   * replaced by other utility if needed
   *
   * @public
   * @param {Node[]} line list of nodes
   * @returns {string}
   */
  lineToString: function(line) {
    var hasInvalidNode = false;
    var ret = "";
    _.each(line, function(node){
      if(node.value === "-1") {
        hasInvalidNode = true;
        ret += "?";
      } else {
        ret += node.value;
      }
    });

    if(hasInvalidNode) {
      ret += " ILLEGAL";
    }

    return ret;
  }

};