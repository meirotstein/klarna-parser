var _ = require('lodash');

/**
 * Token types
 * @enum {string}
 * @readonly
 */
var types = {
  LEFT: "l",                  //"|  "
  RIGHT: "r",                 //"  |"
  BOTTOM: "b",                //" _ "
  LEFT_RIGHT: "lr",           //"| |"
  LEFT_BOTTOM: "lb",          //"|_ "
  LEFT_BOTTOM_RIGHT: "lbr",   //"|_|"
  BOTTOM_RIGHT: "br",         //" _|"
  BLANK: "blnk",              //"   "
  NEWLINE: "nwln",            //"\n" or "\r\n"
  INVALID: "inv"              //other
};

/**
 * Statuses for scanning results
 * @enum {string}
 * @readonly
 */
var statuses = {
  OK: "ok",
  ERROR: "error"
}

/**
 * Token class
 * Represents a set of characters in the document
 * @class
 * @param {string} type token type
 * @constructor
 */
var Token = function(type) {
  this.type = type;
}

/**
 * The grammar tree defines how to convert a set of characters into a token
 * For a certain set there will be a specific token type
 * o/w the set is not recognized in our 'language'
 * @type {Object}
 * @readonly
 */
var grammarTree = {
  " " : {
    " " : {
      "|" : types.RIGHT,
      " " : types.BLANK
    },
    "_" : {
      "|" : types.BOTTOM_RIGHT,
      " " : types.BOTTOM
    }
  },
  "|" : {
    " " : {
      "|" : types.LEFT_RIGHT,
      " " : types.LEFT,
    },
    "_" : {
      "|" : types.LEFT_BOTTOM_RIGHT,
      " " : types.LEFT_BOTTOM
    }
  },
  "\n" : types.NEWLINE, //linux style newline
  "\r" : {
    "\n" : types.NEWLINE  //windows style newline
  }
};

/**
 * Tokenizer converts set of characters into a set of valid token in our 'language'
 * @class
 * @public
 * @constructor
 */
var Tokenizer = function() {};

/**
 * externalize types
 * @public
 * @type {string}
 */
Tokenizer.TYPES = types;

/**
 * externalize statuses
 * @public
 * @type {string}
 */
Tokenizer.STATUSES = statuses;

/**
 * externalize Token
 * @public
 * @class
 */
Tokenizer.Token = Token;

/**
 * scan the given characters and map is into a set of tokens according to the grammarTree
 * the function uses a cursor that for each char moves on the grammarTree and creates a relevant Token once
 * it get into a 'leaf' node
 * @public
 * @param {string} chars
 * @returns {{tokens: Token[], status: (string)}}
 */
Tokenizer.prototype.scan = function(chars) {
  var tokens = [];
  var currentCharStateCursor = grammarTree;
  var status = statuses.OK;

  _.each(chars, function(char){

    currentCharStateCursor = currentCharStateCursor[char];

    if(!_.isObject(currentCharStateCursor)) {  //leaf or undefined

      if(!currentCharStateCursor) {
        tokens.push(new Token(types.INVALID));
        status = statuses.ERROR;
      } else {
        tokens.push(new Token(currentCharStateCursor));
      }

      currentCharStateCursor = grammarTree;
    }
  });

  return {
    tokens: tokens,
    status: status
  };
};

module.exports = Tokenizer;