var _ = require('lodash'),
  chai = require("chai"),
  expect = chai.expect,
  tokenizer = require('../lib/Tokenizer'),
  parser = require('../lib/Parser');

describe('unit tests', function() {

  describe('Tokenizer tests', function(){

    var Tokenizer;

    before(function() {
      Tokenizer = new tokenizer();
    });

    it("empty input should return empty list of tokens" , function(){
      var result = Tokenizer.scan("");
      expect(result.status).to.equal(tokenizer.STATUSES.OK);
      expect(result.tokens.length).to.equal(0);
    });

    _.each([
      {expression:"   ", expected: tokenizer.TYPES.BLANK},
      {expression:"|  ", expected: tokenizer.TYPES.LEFT},
      {expression:"|_ ", expected: tokenizer.TYPES.LEFT_BOTTOM},
      {expression:"|_|", expected: tokenizer.TYPES.LEFT_BOTTOM_RIGHT},
      {expression:" _|", expected: tokenizer.TYPES.BOTTOM_RIGHT},
      {expression:"  |", expected: tokenizer.TYPES.RIGHT},
      {expression:" _ ", expected: tokenizer.TYPES.BOTTOM},
      {expression:"\n", expected: tokenizer.TYPES.NEWLINE},
      {expression:"\r\n", expected: tokenizer.TYPES.NEWLINE}
    ],function(testData) {
      it("test that '" + testData.expected + "' token is returned for given characters", function(){
        var result = Tokenizer.scan(testData.expression);
        expect(result.status).to.equal(tokenizer.STATUSES.OK);
        expect(result.tokens.length).to.equal(1);
        expect(result.tokens[0].type).to.equal(testData.expected);
      });
    });

    it("test of combination of recognized tokens should return list of tokens" , function(){
      var result = Tokenizer.scan(" _ | ||_|");
      expect(result.status).to.equal(tokenizer.STATUSES.OK);
      expect(result.tokens.length).to.equal(3);
      expect(result.tokens[0].type).to.equal(tokenizer.TYPES.BOTTOM);
      expect(result.tokens[1].type).to.equal(tokenizer.TYPES.LEFT_RIGHT);
      expect(result.tokens[2].type).to.equal(tokenizer.TYPES.LEFT_BOTTOM_RIGHT);
    });

    it("unrecognized input should return invalid token" , function(){
      var result = Tokenizer.scan("||");
      expect(result.status).to.equal(tokenizer.STATUSES.ERROR);
      expect(result.tokens.length).to.equal(1);
      expect(result.tokens[0].type).to.equal(tokenizer.TYPES.INVALID);
    });

    it("combination of recognized and unrecognized tokens should return mix of relevant tokens" , function(){
      var result = Tokenizer.scan(" _ | ||||_|");
      expect(result.status).to.equal(tokenizer.STATUSES.ERROR);
      expect(result.tokens.length).to.equal(4);
      expect(result.tokens[0].type).to.equal(tokenizer.TYPES.BOTTOM);
      expect(result.tokens[1].type).to.equal(tokenizer.TYPES.LEFT_RIGHT);
      expect(result.tokens[2].type).to.equal(tokenizer.TYPES.INVALID);
      expect(result.tokens[3].type).to.equal(tokenizer.TYPES.LEFT_BOTTOM_RIGHT);
    });

    it("test combination of recognized tokens with linux newline" , function(){
      var result = Tokenizer.scan(" _ \n| |");
      expect(result.status).to.equal(tokenizer.STATUSES.OK);
      expect(result.tokens.length).to.equal(3);
      expect(result.tokens[0].type).to.equal(tokenizer.TYPES.BOTTOM);
      expect(result.tokens[1].type).to.equal(tokenizer.TYPES.NEWLINE);
      expect(result.tokens[2].type).to.equal(tokenizer.TYPES.LEFT_RIGHT);
    });

    it("test combination of recognized tokens with windows newline" , function(){
      var result = Tokenizer.scan(" _ \r\n| |");
      expect(result.status).to.equal(tokenizer.STATUSES.OK);
      expect(result.tokens.length).to.equal(3);
      expect(result.tokens[0].type).to.equal(tokenizer.TYPES.BOTTOM);
      expect(result.tokens[1].type).to.equal(tokenizer.TYPES.NEWLINE);
      expect(result.tokens[2].type).to.equal(tokenizer.TYPES.LEFT_RIGHT);
    });

  });

  describe('Parser tests', function(){

    var Parser;
    var t =  tokenizer.TYPES;

    before(function() {
      Parser = new parser();
    });

    it("test sequence of tokens representing 123456789 serial number", function () {

      var index = 0;
      var tokens = [];

      _.each(
        [ t.BLANK, t.BOTTOM, t.BOTTOM, t.BLANK, t.BOTTOM, t.BOTTOM, t.BOTTOM, t.BOTTOM, t.BOTTOM, t.NEWLINE,
          t.RIGHT, t.BOTTOM_RIGHT, t.BOTTOM_RIGHT, t.LEFT_BOTTOM_RIGHT, t.LEFT_BOTTOM, t.LEFT_BOTTOM, t.RIGHT, t.LEFT_BOTTOM_RIGHT, t.LEFT_RIGHT, t.NEWLINE,
          t.RIGHT, t.LEFT_BOTTOM, t.BOTTOM_RIGHT, t.RIGHT, t.BOTTOM_RIGHT, t.LEFT_BOTTOM_RIGHT, t.RIGHT, t.LEFT_BOTTOM_RIGHT, t.LEFT_BOTTOM_RIGHT, t.NEWLINE,
          t.NEWLINE]
        ,function(tokenType) {
          tokens.push(new tokenizer.Token(tokenType, index));
          index += tokenType === t.NEWLINE ? 1 : 3;
        });

      var result = Parser.parse(tokens);
      expect(result.lines.length).to.equal(1);
      expect(result.lines[0].length).to.equal(9);
      expect(result.lines[0][0].value).to.equal("1");
      expect(result.lines[0][1].value).to.equal("2");
      expect(result.lines[0][2].value).to.equal("3");
      expect(result.lines[0][3].value).to.equal("4");
      expect(result.lines[0][4].value).to.equal("5");
      expect(result.lines[0][5].value).to.equal("6");
      expect(result.lines[0][6].value).to.equal("7");
      expect(result.lines[0][7].value).to.equal("8");
      expect(result.lines[0][8].value).to.equal("0");

    });

    it("test sequence of tokens representing serial number with mistakes", function () {

      var index = 0;
      var tokens = [];

      _.each(
        [ t.BLANK, t.BOTTOM, t.BOTTOM, t.BLANK, t.BOTTOM, t.BOTTOM, t.BOTTOM, t.BOTTOM, t.BOTTOM, t.NEWLINE,
          t.RIGHT, t.BOTTOM_RIGHT, t.BOTTOM, t.LEFT_BOTTOM_RIGHT, t.LEFT_BOTTOM, t.LEFT_BOTTOM, t.LEFT_RIGHT, t.LEFT_BOTTOM_RIGHT, t.LEFT_RIGHT, t.NEWLINE,
          t.RIGHT, t.LEFT_BOTTOM, t.BOTTOM, t.RIGHT, t.BOTTOM_RIGHT, t.LEFT_BOTTOM_RIGHT, t.RIGHT, t.LEFT_BOTTOM_RIGHT, t.LEFT_BOTTOM_RIGHT, t.NEWLINE,
          t.NEWLINE]
        ,function(tokenType) {
          tokens.push(new tokenizer.Token(tokenType, index));
          index += tokenType === t.NEWLINE ? 1 : 3;
        });

      var result = Parser.parse(tokens);
      expect(result.lines.length).to.equal(1);
      expect(result.lines[0].length).to.equal(9);
      expect(result.lines[0][0].value).to.equal("1");
      expect(result.lines[0][1].value).to.equal("2");
      expect(result.lines[0][2].value).to.equal(parser.NODE_INVALID);
      expect(result.lines[0][3].value).to.equal("4");
      expect(result.lines[0][4].value).to.equal("5");
      expect(result.lines[0][5].value).to.equal("6");
      expect(result.lines[0][6].value).to.equal(parser.NODE_INVALID);
      expect(result.lines[0][7].value).to.equal("8");
      expect(result.lines[0][8].value).to.equal("0");

    });




  });

});
