var _ = require('lodash'),
  chai = require('chai'),
  expect = chai.expect,
  fs = require("fs"),
  path = require('path'),
  readline = require('readline'),
  klarnaParser = require('../index'),
  utils = require('../lib/Utils');

describe('e2e scenarios tests', function() {

  var INPUT_DIR = path.join("test" , "inputs");
  var OUTPUT_DIR = path.join("test" , "outputs");

  var KlarnaParser;

  before(function() {
    KlarnaParser = new klarnaParser();
  });

  _.each([
    {input: "input_user_story_1.txt", output: "output_user_story_1.txt"},
    {input: "input_user_story_2.txt", output: "output_user_story_2.txt"}
  ], function(testData) {
    it("parsing results of " + testData.input + " should equal to content of " + testData.output, function(done){
      return utils.readFile(path.join(INPUT_DIR , testData.input)).then(function(inputContent){
        var results = KlarnaParser.parse(inputContent);
        var currentLine = 0;

        var lineReader = readline.createInterface({
          input: fs.createReadStream(path.join(OUTPUT_DIR , testData.output))
        });

        lineReader.on('line', function (line) {
          expect(currentLine).to.be.below(results.lines.length);
          expect(utils.lineToString(results.lines[currentLine++])).to.equal(line);
        });

        lineReader.on('close', function (line) {
          expect(currentLine).to.equal(results.lines.length);
          done();
        });


      });
    })

  });

  it("parsing wrong content should end the parsing process with no results", function(){
    var results = KlarnaParser.parse(' _  _ ||| _ ');
    expect(results).to.be.undefined;
  });

});