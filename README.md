# Klarna Parser

### Overview

In this solutions I have defined two simple structures:

**Token** - represents a single symbol - it transforms from a set of characters (usually three).  
for instance: the set of the following characters '|_|' will transform into a Token of type **LEFT_BOTTOM_RIGHT**  

**Node** - represents a single 'word' - in our case it will be a number. it transforms from a set of tokens.  
for instance: a set of Tokens of types: **[BOTTOM, RIGHT, RIGHT]** will transform to the word "7"

The solution separates the parsing flow into two phases:

**The first** scans the text and transform the characters into a list of **Tokens** - the scan is done by using grammar tree structure.  
Done by the **Tokenizer** component.

**the second** goes over the Tokens - the first, second and third sets of 10 Tokens are considered as a rows while the forth row contains only single token of type **NEWLINE**.  
For each of the first three rows a cursor is placed over the first token and the three cursors moved simultaneously over the three rows - on every move a **Node** is created from to the currently pointed Tokens by using a syntax tree structure.  
Once it is done a line of **Nodes** are created which represents an invoice number and the parsing continues to the next Tokens, if available.  
Done by the **Parser** component

### Installation

1. Clone this repo
2. Run 'npm install'

### Usage

```bash
node klarna-parser input_file [output_file]
```
* The input_file is a full path of the input document - it is mandatory
* The output_file is a full path of the desired output file - if not given a new *input_file.output* will be created

### Tests and coverage

The code is 100% tested. the coverage report is also available under the *coverage* folder

You can use grunt to run the tests and re-generate the coverage report by simply running the *grunt* command under the root folder