var
  _ = require('lodash'),
  Tokenizer = require('./lib/Tokenizer'),
  Parser = require('./lib/Parser');

/**
 * Main module class
 *
 * @class
 * @public
 * @constructor
 */
KlarnaParser = function() {
    /** Tokenizer */this.tokenizer = new Tokenizer();
    /** Parser */ this.parser = new Parser();
}

/**
 * Parse a text of 7-segment display representations of the invoice numbers into matrix of nodes
 * each row in the matrix represents an invoice number
 * Flow:
 * (1) the text if scanned by the tokenizer and resolved into a set of tokens
 * (2) the tokens are parsed by the parser and resolved into a matrix of nodes
 *
 * @param {string} invoiceNumbersText
 * @returns {lines: Array<Node[]>}
 */
KlarnaParser.prototype.parse = function(invoiceNumbersText) {
    var tokenResult = this.tokenizer.scan(invoiceNumbersText);

    if(tokenResult.status == Tokenizer.STATUSES.ERROR) {
        console.log("unrecognised tokens found");
        return;
    }

    return this.parser.parse(tokenResult.tokens);

};

module.exports = KlarnaParser;