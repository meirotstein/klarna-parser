/**
 * This script wrap the klarna-parser module and allow to run it via command line
 *
 * usage:
 * node klarna-parser input_file [output_file]
 * if no output_file is given a new input_file.output file will be created
 */

var
  path = require('path'),
  fs = require('fs'),
  utils = require('./lib/Utils'),
  klarnaParser = require('../klarna-parser');

if(process.argv.length < 3 || process.argv.length > 4) {
  console.log("usage: node klarna-parser input_file [output_file]");
  return;
}

var inputFilePath = process.argv[2];
var outputFilePath;

if(process.argv[3]) {
  outputFilePath = process.argv[3];
} else {
  var inputParts = path.parse(inputFilePath);
  outputFilePath = path.join(inputParts.dir, inputParts.name + '.output' + inputParts.ext);
}


utils.readFile(inputFilePath).then(function(inputContent){
  console.log("parsing file content...");
  var KlarnaParser = new klarnaParser();
  var results = KlarnaParser.parse(inputContent);
  if(results) {
    console.log("writing results to " + outputFilePath + "...");
    var outputFile = fs.createWriteStream(outputFilePath);
    outputFile.on('error', function(err) {
      console.log('Error while writing to ' + outputFilePath + "\n" + err.message);
    });
    results.lines.forEach(function(line){
      outputFile.write(utils.lineToString(line) + '\n');
    });
    console.log("done!");
  } else {
    console.log("parsing ended with no results");
  }
}, function(err){
  console.log('Error while reading from: ' + inputFilePath + "\n" + err.message);
});