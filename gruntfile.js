module.exports = function (grunt) {
	"use strict";

	grunt.initConfig({
		mocha_istanbul: {
			coverage: {
				src: 'test', // a folder works nicely
				options: {
					mask: '*Spec.js'
				}
			}
		},
		istanbul_check_coverage: {
			default: {
				options: {
					coverageFolder: 'coverage*', // will check both coverage folders and merge the coverage results
					check: {
						lines: 98,
						statements: 98,
						functions: 98,
						branches: 98
					}
				}
			}
		}

	});

	grunt.loadNpmTasks('grunt-mocha-istanbul');

	grunt.registerTask('default', ['mocha_istanbul:coverage','istanbul_check_coverage']);

};
